# concensum-registrars

The goal of this project is to associate names with *registrar addresses*.

Concensum '**Register**' feature allows users to store their data on the blockchain as a key-value store.

When you store *records* in the blockchain, you store them against an address, which is called the '*registrar address*'. The '*registrar*' is basically the author of a record.

Sometimes it might be useful to give a public name to a *registrar address*.

> For example, www.concensum.net uses this list to name the registrar when showing a register record:
> 
> https://www.concensum.net/tx/ce707cad38bfbff0a7475fb8d1c7649fb1f9bf1b051502fd23b7ad8d570cbee4


To add new addresses to this list a file should be created, per address:

*<the_registrar_address_pubkeyhash>.json*
```
{
  "name": "The name to appear"
}
```

If you want to include your own address on our list submit a merge request. We will handle every case individually.

Keep in mind that we can at any time remove or add new addresses without warning.